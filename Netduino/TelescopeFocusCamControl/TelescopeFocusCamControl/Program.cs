﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;
//using SerialPortLearn;
using System.Reflection;

namespace TelescopeFocusCamControl
{
    public class Program
    {
        public static void Main()
        {
            MainProg start = new MainProg();
            start.Start();
        }
    }
}
