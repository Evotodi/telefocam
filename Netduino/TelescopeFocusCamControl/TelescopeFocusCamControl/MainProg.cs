using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;
//using SerialPortLearn;
using System.Reflection;
using System.IO.Ports;
using System.Text;


namespace TelescopeFocusCamControl
{
    class MainProg
    {
        //gear ratio is 11.5:1
        public const int maxSteps = 176640; // set to max number of steps from stop to stop when in the highest resolution step mode e.x. 40 rot * 11.5 ratio = 460 motor rot * spd1 steps per rot = 176640 steps
        public const int spd1 = 384; //steps per rotation  // 1/8
        public const int spd2 = 192; //steps per rotation  // 1/4
        public const int spd3 = 48; //steps per rotation   // 1/1

        public const int spd3mply = 8; // =spd3 / spd1
        public const int spd2mply = 2; // =spd3 / spd2
        public const int spd1mply = 1; // =spd3 / spd3


        public const double max_step_speed = 100; //max delay between steps

        public const double map_fromSource = 1000;
        public const double map_toSource = 0;

        public const bool debug_skip_home = false;

        public Thread _Stepper;
        public static Thread _Run;
        public Thread _LCDUpdate;
        public Thread _Comms;
        
        public int stepspeed = 0;
        public bool dir;
        //public bool focus;
        public bool shutter;
        public int motorspeed;
        public string version = "";
        public int stepcounter = 0;
        //public bool motorenable = true;
        public bool homeSwOpen = true;
        public int bulb;
        public bool cammode = false;
        public int[] threads = { 0, 0, 0, 0 };

        const int bufferMax = 1024;
        static byte[] buffer = new Byte[bufferMax];
        static int bufferLength = 0;

        LXIT.Hardware.SerLCD LCD;
        //SerialPortHelper Ser;
        SerialPort Ser = new SerialPort(Serial.COM2, 57600, Parity.None, 8, StopBits.One);

        InputPort HomeSW = new InputPort(Pins.GPIO_PIN_D10, false, Port.ResistorMode.Disabled);
        //InputPort DataReady = new InputPort(Pins.GPIO_PIN_D11, false, Port.ResistorMode.Disabled);
        
        
        OutputPort OnboardLed = new OutputPort(Pins.ONBOARD_LED, false);
        OutputPort CommReset = new OutputPort(Pins.GPIO_PIN_A1,false);
        OutputPort CamFocus = new OutputPort(Pins.GPIO_PIN_A2, false);
        OutputPort CamShutter = new OutputPort(Pins.GPIO_PIN_A3, false);
        OutputPort MotorDir = new OutputPort(Pins.GPIO_PIN_D4, false);
        OutputPort MotorStep = new OutputPort(Pins.GPIO_PIN_D5, false);
        OutputPort MotorSpd1 = new OutputPort(Pins.GPIO_PIN_D6, false);
        OutputPort MotorSpd2 = new OutputPort(Pins.GPIO_PIN_D7, false);
        OutputPort MotorSpd3 = new OutputPort(Pins.GPIO_PIN_D8, false);
        //OutputPort MotorEnable = new OutputPort(Pins.GPIO_PIN_D9, false);
        //OutputPort ReadyRx = new OutputPort(Pins.GPIO_PIN_D12, false);
        

        public void Start()
        {
            Debug.Print("Resetting Comms");
            CommReset.Write(true);

            Ser.Open();

            _Run = new Thread(delegate() { Run(); });
            threads[0] = _Run.ManagedThreadId;
            Debug.Print("Starting Main Program");
            _Run.Start();
            

            Assembly asm = Assembly.GetExecutingAssembly();
            AssemblyName assemblyName = asm.GetName();
            version = assemblyName.Version.ToString();
            Debug.Print(version.ToString());

        }
        public void Run()
        {
            //Testing motor steps
            //int x = 386;
            //SetSpd1();
            //for (int y = 0; y <= 3; y++)
            //{
            //    for (int i = 1; i <= x; i++)
            //    {
            //        dostep(true, 2);
            //    }
            //    x = x - 2;
            //    Thread.Sleep(3000);
            //}
            //throw new Exception("exit");

            if (Setup())
            {
                Debug.Print("Setup Complete");
            }

            Debug.Print("Starting Communications");
            _Comms = new Thread(delegate() { Comms(); });
            threads[3] = _Comms.ManagedThreadId;
            _Comms.Start();

            Debug.Print("Starting Stepper Control");
            _Stepper = new Thread(delegate() { Stepper(); });
            threads[1] = _Stepper.ManagedThreadId;
            _Stepper.Start();

            Debug.Print("Starting Display Control");
            _LCDUpdate = new Thread(delegate() { LCDUpdate(); });
            threads[2] = _LCDUpdate.ManagedThreadId;
            _LCDUpdate.Start();          

            Debug.Print("Run Thread: " + threads[0].ToString());
            Debug.Print("Stepper Thread: " + threads[1].ToString());
            Debug.Print("LCD Thread: " + threads[2].ToString());
            Debug.Print("Comms Thread: " + threads[3].ToString());

        }
        public bool Setup()
        {
            FlashLED(250);

            //ReadyRx.Write(true);
            //Thread.Sleep(250);
            //ReadyRx.Write(false);

            stepspeed = 0;
            motorspeed = 1;
            //motorenable = false;
            dir = true;
            //focus = false;
            shutter = false;
            bulb = 0;
            
            LCD = new LXIT.Hardware.SerLCD("COM1");

            //LCD.Write(new byte[] { 0xFE, 0x40 });
            //LCD.Write("                                        "); //Clears LCD Startup Screen

            LCD.ClearDisplay();
            LCD.SetCursor(1, 1);
            LCD.Write("  Telescope FoCam");
            LCD.SetCursor(1, 2);
            LCD.Write("      " + version);
            Thread.Sleep(3000);
            
            LCD.SetCursor(1, 2);
            LCD.Write("Connecting Hand Ctrl");

            Debug.Print("Waiting on comms");
            string temp = "";
            bool connected = false;
            SerPrintLine("master");
            //CommReset.Write(false);
            while (connected == false)
            {
                try
                {
                    SerPrintLine("master");
                    temp = getResponse(Ser);
                    if (temp.Length > 0)
                    {
                        Debug.Print("Data Rcvd: " + temp);
                    }
                    Thread.Sleep(250);
                    if (temp == "comms")
                    {
                        connected = true;
                        Debug.Print("Connected");
                    }
                    temp = "";
                }
                catch (Exception ex)
                {
                    Debug.Print("Void Setup 'Waiting for arduino': " + ex.Message);
                    SerPrintLine("master");
                }
            }
            SerPrintLine("a");
            Thread.Sleep(2000);


            LCD.ClearDisplay();
            LCD.Write(new byte[] { 0xFE, 0x68 });
            LCD.Write(new byte[] { 0xFE, 0x7C, 11, 1, 0, 0 });
            LCD.SetCursor(1, 1);
            LCD.Write("Focus:  0%");
            LCD.SetCursor(1, 2);
            LCD.Write("Cam:Closed Bulb:600s");
            Thread.Sleep(200);
            LCD.SetCursor(1, 1); 
            LCD.Write("Focus: 25%");
            LCD.Write(new byte[] { 0xFE, 0x7C, 11, 1, 0, 13 });
            Thread.Sleep(200);
            LCD.SetCursor(1, 1);
            LCD.Write("Focus: 50%");
            LCD.Write(new byte[] { 0xFE, 0x7C, 11, 1, 0, 25 });
            Thread.Sleep(200);
            LCD.SetCursor(1, 1);
            LCD.Write("Focus: 75%");
            LCD.Write(new byte[] { 0xFE, 0x7C, 11, 1, 0, 38 });
            Thread.Sleep(200);
            LCD.SetCursor(1, 1);
            LCD.Write("Focus:100%");
            LCD.Write(new byte[] { 0xFE, 0x7C, 11, 1, 0, 50 });           
            Thread.Sleep(200);
            LCD.SetCursor(1, 2);
            LCD.Write("Cam:Opened Bulb:600s");
            Thread.Sleep(400);
            LCD.SetCursor(1, 2);
            LCD.Write("Cam:Closed Bulb:  0s");
            Thread.Sleep(200);
            LCD.Write(new byte[] { 0xFE, 0x7C, 11, 1, 0, 0 }); 
            LCD.SetCursor(1, 1);
            LCD.Write("Focus:  Homing...");                 
            home();
            Thread.Sleep(200);
            LCD.SetCursor(1, 1);
            LCD.Write("Focus:  0%");
            LCD.Write(new byte[] { 0xFE, 0x7C, 11, 1, 0, 0 });
            LCD.SetBrightness(15);
            return true;
        }

        public string SerInput(int timeout = 0)
        {
            long starttime = (Utility.GetMachineTime().Ticks / 10000);
            long endtime = starttime + timeout;
            string input = "";
            bool read = false;
            while (read == false)
            {
                if (timeout != 0)
                {
                    if ((Utility.GetMachineTime().Ticks / 10000) > endtime)
                    {
                        throw new Exception("Timeout Reached");
                    }
                }
                string line = SerReadLine();
                if (line.Length > 0)
                {
                    input = line;
                    read = true;
                }
            }
            return input;
        }
        public string SerReadLine()
        {
            string line = "";

            lock (buffer)
            {
                //-- Look for Return char in buffer --
                for (int i = 0; i < bufferLength; i++)
                {
                    //-- Consider EITHER CR or LF as end of line, so if both were received it would register as an extra blank line. --
                    if (buffer[i] == '\r' || buffer[i] == '\n')
                    {
                        buffer[i] = 0; // Turn NewLine into string terminator
                        line = "" + new string(Encoding.UTF8.GetChars(buffer)); // The "" ensures that if we end up copying zero characters, we'd end up with blank string instead of null string.
                        //Debug.Print("LINE: <" + line + ">");
                        bufferLength = bufferLength - i - 1;
                        Array.Copy(buffer, i + 1, buffer, 0, bufferLength); // Shift everything past NewLine to beginning of buffer
                        break;
                    }
                }
            }

            return line;
        }
        public void SerPrint(string line)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            byte[] bytesToSend = encoder.GetBytes(line);
            Ser.Write(bytesToSend, 0, bytesToSend.Length);
        }
        public void SerPrintLine(string line)
        {
            //Print(line + "\r\n");
            SerPrint(line + "\n");
        }
        public void SerPrintClear()
        {
            for (int i = 1; i <= 100; i++)
            {
                SerPrintLine("");
            }
        }
        public void FlashLED(int delay = 5)
        {
            OnboardLed.Write(true);
            Thread.Sleep(delay);
            OnboardLed.Write(false);
        }
        public void SetOutputs()
        {
            //if (motorenable)
            //{
            //    MotorEnable.Write(true);
            //}
            //else
            //{
            //    MotorEnable.Write(false);
            //}
            //if (focus)
            //{
            //    CamFocus.Write(true);
            //}
            //else
            //{
            //    CamFocus.Write(false);
            //}
            if (shutter)
            {
                CamShutter.Write(true);
            }
            else
            {
                CamShutter.Write(false);
            }
            if (dir)
            {
                MotorDir.Write(true);
            }
            else
            {
                MotorDir.Write(false);
            }
        }
        private void Stepper()
        {
            bool validstep = true;
            int spdmply = 0;
            while(true)
            {
                SetOutputs();

                if (stepspeed != max_step_speed)
                {
                    switch (motorspeed)
                    {
                        case 1:
                            spdmply = spd1mply;
                            SetSpd1();
                            break;
                        case 2:
                            spdmply = spd2mply;
                            SetSpd2();
                            break;
                    }

                    if (dir)
                    {
                        if (stepcounter >= maxSteps)
                        {
                            stepcounter = maxSteps;
                        }
                        else
                        {
                            stepcounter = stepcounter + (1 * spdmply);                            
                        }
                    }
                    else
                    {
                        if (stepcounter <= 0)
                        {
                            stepcounter = 0;
                        }
                        else
                        {
                            stepcounter = stepcounter - (1 * spdmply);
                        }
                    }

                    if (dir && stepcounter == maxSteps)
                    {
                        validstep = false;
                    }
                    
                    if (!dir && stepcounter == 0)
                    {
                        validstep = false;
                    }

                    if (validstep)
                    {
                        dostep(dir, stepspeed);
                    }
                    validstep = true;
                }
            }
        }

        //private void Stepper()
        //{
        //    bool validstep = true;
        //    int spdval = 0;
        //    int spdmply = 0;
        //    while (true)
        //    {
        //        SetOutputs();

        //        if (stepspeed != max_step_speed)
        //        {
        //            switch (motorspeed)
        //            {
        //                case 1:
        //                    spdval = spd1;
        //                    spdmply = spd1mply;
        //                    SetSpd1();
        //                    break;
        //                case 2:
        //                    spdval = spd2;
        //                    spdmply = spd2mply;
        //                    SetSpd2();
        //                    break;
        //            }

        //            if (dir)
        //            {
        //                if (stepcounter + spdval >= maxSteps)
        //                {
        //                    stepcounter = maxSteps;
        //                }
        //                else
        //                {
        //                    stepcounter = stepcounter + spdval;
        //                }
        //            }
        //            else
        //            {
        //                if (stepcounter - spdval <= 0)
        //                {
        //                    stepcounter = 0;
        //                }
        //                else
        //                {
        //                    stepcounter = stepcounter - spdval;
        //                }
        //            }

        //            if (dir && stepcounter == maxSteps)
        //            {
        //                validstep = false;
        //            }

        //            if (!dir && stepcounter == 0)
        //            {
        //                validstep = false;
        //            }

        //            if (validstep)
        //            {
        //                dostep(dir, stepspeed);
        //            }
        //            validstep = true;
        //        }
        //    }
        //}


        private bool dostep(bool mdir, int dly)
        {
            if (dly < 0)
            {
                dly = 1;
            }
            MotorDir.Write(mdir);
            MotorStep.Write(true);
            Thread.Sleep(dly);
            MotorStep.Write(false);            
            return true;
        }
        private void home()
        {
            Debug.Print("Homing Scope");
            if (debug_skip_home)
            {
                Debug.Print("DEBUG SKIPPING HOMING");
                stepcounter = maxSteps / 2;
            }
            else
            {

                bool homesw = true;//HomeSW.Read();
                bool athome = true;
                int longrun = 1;

                do
                {
                    Debug.Print("Finding Home switch");
                    homesw = HomeSW.Read();
                    Debug.Print("HomeSW: " + homesw);
                    SetSpd3();
                    MotorDir.Write(true);//IN
                    //MotorEnable.Write(true);
                    while (homesw == false)
                    {
                        homesw = HomeSW.Read();
                        MotorStep.Write(true);
                        MotorStep.Write(false);
                        Thread.Sleep(1);
                    }


                    Debug.Print("Home switch found");
                    Debug.Print("HomeSW: " + homesw);
                    Debug.Print("Sleeping 1 sec");
                    Thread.Sleep(1000);
                    MotorDir.Write(false);//OUT
                    SetSpd3();
                    while (homesw == true)
                    {
                        MotorStep.Write(true);
                        MotorStep.Write(false);
                        Thread.Sleep(1);
                        homesw = HomeSW.Read();
                        longrun++;
                        if (longrun >= 2000)
                        {
                            SetSpd3();
                        }
                    }



                    athome = HomeSW.Read();
                    Debug.Print("Homed");
                    Thread.Sleep(3000);
                } while (athome);
                Debug.Print("Steps: " + longrun);
                SetSpd3();
                MotorDir.Write(true);
                for (int i = 1; i <= (maxSteps / spd3mply) / 2; i++)
                {
                    MotorStep.Write(true);
                    MotorStep.Write(false);
                    Thread.Sleep(1);
                }
                stepcounter = maxSteps / 2;
            }
        }
        private void SetSpd1() //
        {
            MotorSpd1.Write(true);
            MotorSpd2.Write(true);
            MotorSpd3.Write(false);
        }
        private void SetSpd2() //
        {
            MotorSpd1.Write(false);
            MotorSpd2.Write(true);
            MotorSpd3.Write(true);
        }
        private void SetSpd3() //Full step
        {
            MotorSpd1.Write(false);
            MotorSpd2.Write(false);
            MotorSpd3.Write(true);
        }
        private void LCDUpdate()
        {
            bool shutterprev = shutter;
            //bool focusprev = focus;
            bool cammodeprev = cammode;
            int bulbprev = bulb;
            //bool motorenableprev = motorenable;
            int stepcounterprev = stepcounter;
            bool update = false;
            int percent = 0;
            string LCDPercNum = "  0";
            string LCDBulb = "  0";
            string LCDCam = "Closed";
            int i = 0;
            double temp1;


            while (true)
            {
                temp1 = 50.0 * ( ((double)stepcounter / (double)maxSteps));
                percent = (int)temp1;
                i = (int)temp1 * 2;
                LCDPercNum = i.ToString();

                LCDBulb = bulb.ToString();

                if (shutter)
                {
                    LCDCam = "Opened";
                }
                //else if (focus)
                //{
                //    LCDCam = "Focus ";
                //}
                else
                {
                    LCDCam = "Closed";
                }

                if (shutterprev != shutter) { update = true; }
                //if (focusprev != focus) { update = true; }
                if (bulbprev != bulb) { update = true; }
                if (cammodeprev != cammode) { update = true; }
                if (stepcounterprev != stepcounter) { update = true; }
                
                if (update)
                {
                    shutterprev = shutter;
                    //focusprev = focus;
                    bulbprev = bulb;
                    //motorenableprev = motorenable;
                    stepcounterprev = stepcounter;
                    update = false;

                    if (LCDPercNum.Length < 3)
                    {
                        i = LCDPercNum.Length;
                        switch (i)
                        {
                            case 1:
                                LCDPercNum = "  " + LCDPercNum;
                                break;
                            case 2:
                                LCDPercNum = " " + LCDPercNum;
                                break;
                        }
                    }
                    if (LCDBulb.Length < 3)
                    {
                        i = LCDBulb.Length;
                        switch (i)
                        {
                            case 1:
                                LCDBulb = "  " + LCDBulb;
                                break;
                            case 2:
                                LCDBulb = " " + LCDBulb;
                                break;
                        }
                    }
                    //if (LCDCam.Length < 6)
                    //{
                    //    i = LCDCam.Length;
                    //    while (i < 6)
                    //    {
                    //        i = LCDCam.Length;
                    //        LCDCam = LCDCam + " ";
                    //    }
                    //}
                    LCD.ClearDisplay();
                    LCD.Write(new byte[] { 0xFE, 0x7C, 11, 1, 0, (byte)percent });
                    LCD.SetCursor(1, 1);
                    LCD.Write("Focus:"+LCDPercNum+"%");
                    LCD.SetCursor(1, 2);
                    if (!cammode)
                    {
                        LCD.Write("Cam:" + LCDCam + " Bulb:" + LCDBulb + "s");
                    }
                    else
                    {
                        LCD.Write("Cam:" + LCDCam + " Bulb: MAN");
                    }
                }
                Thread.Sleep(200);
            }
        }
        private void Comms()
        {
            double speed_temp = 0;
            string serData = "";
            int x, y, b, c = 0;

            while (true)
            {
                serData = getResponse(Ser);

                if (serData.IndexOf("B") == -1)
                {
                    //no start char found
                    //look again
                    Thread.Sleep(50);
                    continue;
                }

                b = serData.IndexOf("B");
                c = serData.IndexOf("C");

                //Step Delay
                x = serData.IndexOf("S") + 1;
                y = serData.IndexOf("D");
                speed_temp = double.Parse(serData.Substring(x, y - x));
                //Debug.Print("S:" + serData.Substring(x, y - x));
                speed_temp = (speed_temp - map_fromSource) / (map_toSource - map_fromSource) * max_step_speed;
                stepspeed = (int)speed_temp;


                //Direction
                //Debug.Print("D:" + serData.Substring(y + 1, 1));
                if (serData.Substring(y + 1, 1) == "0")
                {
                    dir = false;
                }
                else
                {
                    dir = true;
                }


                //Motor Speed
                x = serData.IndexOf("P");
                //Debug.Print("P:" + serData.Substring(x + 1, 1));
                switch (serData.Substring(x + 1, 1))
                {
                    case "1":
                        motorspeed = 1;
                        break;
                    case "2":
                        motorspeed = 2;
                        break;
                }
                
                //Shutter
                x = serData.IndexOf("K");
                //Debug.Print("K:" + serData.Substring(x + 1, 1));
                if (serData.Substring(x + 1, 1) == "0")
                {
                    shutter = false;
                }
                else
                {
                    shutter = true;
                }

                //Bulb
                x = serData.IndexOf("L") + 1;
                y = serData.IndexOf("M");
                bulb = int.Parse(serData.Substring(x, y - x));
                //Debug.Print("L:" + serData.Substring(x, y - x));

                //Cam Sel
                //Debug.Print("M:" + serData.Substring(y + 1, 1));
                if (serData.Substring(y + 1, 1) == "0")
                {
                    cammode = false;
                }
                else
                {
                    cammode = true;
                }

                //Debug.Print(serData);

            }
        }
        private static string getResponse(SerialPort serial)
        {
            String response = "";
            while (serial.BytesToRead > 0)
            {
                byte[] buf = new byte[1];
                serial.Read(buf, 0, 1);
                // Line feed - 0x0A - marks end of data.
                // Append each byte read until end of data.
                if (buf[0] != 0x0D)
                {
                    response += (char)buf[0];
                }
                else
                {
                    Debug.Print(response);
                    break;
                }
            }
            return response;
        }
    }
}
