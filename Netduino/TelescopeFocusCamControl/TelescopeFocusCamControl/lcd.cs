using System;
using System.IO.Ports;

namespace LXIT.Hardware
{
    public sealed class SerLCD
    {
        public enum Direction { Left, Right }
        public enum DisplayType { C16L2, C16L4, C20L2, C20L4 }
        public enum Status { On, Off }
        private readonly SerialPort _serialPort;
        private readonly DisplayType _displayType;
        public SerLCD(string portName)
            : this(portName, DisplayType.C16L2)
        { }
        public SerLCD(string portName, DisplayType displayType)
        {
            // Defaults for SerialPort are the same as the settings for the LCD, but I'll set them explicitly
            _serialPort = new SerialPort(portName, 19200, Parity.None, 8, StopBits.One);

            _displayType = displayType;
        }
        public void ClearDisplay()
        {
            Write(new byte[] { 0xFE, 0x58 });
        }
        public void MoveHome()
        {
            Write(new byte[] { 0xFE, 0x48 });
        }
        public void MoveCursor(Direction direction)
        {
            MoveCursor(direction, 1);
        }
        public void MoveCursor(Direction direction, int times)
        {
            byte command;

            switch (direction)
            {
                case Direction.Left: command = 0x4C; break;
                case Direction.Right: command = 0x4D; break;
                default: return;
            }

            for (int i = 0; i < times; i++)
            {
                Write(new byte[] { 0xFE, command });
            }
        }
        public void Open()
        {
            if (!_serialPort.IsOpen)
                _serialPort.Open();
        }
        public void SetBlinkingBoxCursor(Status status)
        {
            byte command;

            switch (status)
            {
                case Status.On: command = 0x53; break;
                case Status.Off: command = 0x54; break;
                default: return;
            }

            Write(new byte[] { 0xFE, command });
        }
        public void SetBrightness(int brightness)
        {
            if (brightness < 0 || brightness > 255)
                throw new ArgumentOutOfRangeException("brightness", "Value of brightness must be between 128-157.");

            Write(new byte[] { 0xFE, 0x99, (byte)brightness });
        }
        public void SetCursor(int col, int row)
        {
            
            Write(new byte[] { 0xFE, 0x47, (byte)col, (byte)row });
        }
        public void SetUnderlineCursor(Status status)
        {
            byte command;

            switch (status)
            {
                case Status.On: command = 0x4A; break;
                case Status.Off: command = 0x4B; break;
                default: return;
            }

            Write(new byte[] { 0xFE, command });
        }
        public void Write(byte buffer)
        {
            Write(new[] { buffer });
        }
        public void Write(byte[] buffer)
        {
            Open();

            _serialPort.Write(buffer, 0, buffer.Length);
        }
        public void Write(char character)
        {
            Write((byte)character);
        }
        public void Write(string text)
        {
            byte[] buffer = new byte[text.Length];

            for (int i = 0; i < text.Length; i++)
            {
                buffer[i] = (byte)text[i];
            }

            Write(buffer);
        }
    }
}


