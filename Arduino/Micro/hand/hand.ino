/*
TeleFoCam Hand Controller (hand)

Controller: Arduino Micro
Date : 2/19/2014
Version : 1.0
*/

#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"
#include <Encoder.h>

RF24 radio(10,9);
Encoder myEnc(3, 2);

const boolean debug = false;

int xAxisPin = A0;
int yAxisPin = A1;
int selPin = A2;
int statLedPin = A5;
int camSelPin = A3;

int selPinState = LOW;

int shutter = 0;
int dir = 0;
int spd = 1;
int stepspd = 0;
int bulb = 0;
int bulbtmp = 0;
String bulbstr = "";

const int numAnalogReadings = 50;
int analogTotal = 0;

int xAxisVal = 0;
int yAxisVal = 0;

int xAxisCtrVal = 512;
int yAxisCtrVal = 512;

long encOldPosition  = -999;

int camSel = 0;

String data = "";
String datatmp = "";
String prevData = "";
byte crc;

const uint64_t pipes[2] = { 0xDEDEDEDEE7LL, 0xDEDEDEDEE9LL };

boolean stringComplete = false; // whether the string is complete
static int dataBufferIndex = 0;
boolean stringOverflow = false;
char charOverflow = 0;

char SendPayload[31] = "";
char RecvPayload[31] = "";
char serialBuffer[31] = "";

boolean startup = false; //set to false after dubugging

void setup(void) {
  pinMode(statLedPin, OUTPUT); 
  pinMode(selPin,INPUT_PULLUP);
  pinMode(camSelPin,INPUT);
  
  digitalWrite(statLedPin, HIGH);
  
  Serial1.begin(57600);
  radio.begin();
  printf_begin();
  
  Serial1.println("Hand Controller Begin");
  
  camSel = digitalRead(camSelPin);
  
  //center x
  for (int thisReading = 0; thisReading < numAnalogReadings; thisReading++){
    analogTotal = analogTotal + analogRead(xAxisPin); 
  }
  xAxisCtrVal = constrain(analogTotal / numAnalogReadings,488,536);
  Serial1.print("X Center: ");
  Serial1.println(xAxisCtrVal);
  
  analogTotal = 0;
  
  //center y
  for (int thisReading = 0; thisReading < numAnalogReadings; thisReading++){
    analogTotal = analogTotal + analogRead(yAxisPin); 
  }
  yAxisCtrVal = constrain(analogTotal / numAnalogReadings,488,536);
  Serial1.print("Y Center: ");
  Serial1.println(yAxisCtrVal);
    
  
  
  radio.setDataRate(RF24_250KBPS);
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(70);
  
  radio.enableDynamicPayloads();
  radio.setRetries(15,15);
  radio.setCRCLength(RF24_CRC_16);
  
  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);
  
  radio.startListening();
  radio.printDetails();
  
  digitalWrite(statLedPin, LOW);
  delay(1000);
  
  //radio.printDetails();
  digitalWrite(statLedPin, HIGH);
  Serial1.println();
  Serial1.println("Waiting for comm controller");
  
  while(!startup){
    
    delay(500);
    digitalWrite(statLedPin, LOW);
    delay(500);
    digitalWrite(statLedPin, HIGH);
    
    int len = 0;
    if ( radio.available() ) {
      bool done = false;
      while ( !done ) {
        len = radio.getDynamicPayloadSize();
        done = radio.read(&RecvPayload,len);
        delay(5);
      }
      RecvPayload[len] = 0; // null terminate string
      if(debug){
        Serial1.print("Rcvd:");
        Serial1.print(RecvPayload);
        Serial1.println();
      }
      
      if((String)RecvPayload == "base"){
        Serial1.println("Comm controller ready. Sending response hand ready."); 
        nrf_send("hand");
        startup = true;
      }      
      RecvPayload[0] = 0; // Clear the buffers     
    } 
  } 
  digitalWrite(statLedPin, HIGH);
  delay(250);  

}//end setup

void loop(void) {
  xAxisVal = analogRead(xAxisPin);
  yAxisVal = analogRead(yAxisPin);
  
  stepspd = 0;
    
  if(yAxisVal < yAxisCtrVal){
    dir = 0;
    spd = 1;
    stepspd = map(yAxisVal,yAxisCtrVal,0,0,1000);
  }else if(yAxisVal > yAxisCtrVal){
    dir = 1;
    spd = 1;
    stepspd = map(yAxisVal,yAxisCtrVal,1023,0,1000);
  }else{
    if(xAxisVal < xAxisCtrVal){
       dir = 0;
       spd = 2;
       stepspd = map(xAxisVal,xAxisCtrVal,0,0,1000);
    }else if(xAxisVal > xAxisCtrVal){
       dir = 1;
       spd = 2; 
       stepspd = map(xAxisVal,xAxisCtrVal,1023,0,1000);
    }else{
      stepspd = 0; 
    }
  }
  
  if(stepspd < 3){ //solve some low voltage gitter
    stepspd = 0;  
  }
  
  
  selPinState = digitalRead(selPin);
  if(selPinState == LOW){
    stepspd = 0;
    shutter = 1; 
  }else{
    shutter = 0;
  }
  
  
  long encNewPosition = myEnc.read();
  if (encNewPosition != encOldPosition) {
    encOldPosition = encNewPosition;
  }
  if(encNewPosition < 0){
    myEnc.write(0); 
  }
  encOldPosition = constrain(encOldPosition,0,240);
  bulb = encOldPosition;
  
  camSel = digitalRead(camSelPin);
  
  if(camSel == HIGH){
     bulb = 0;
  }
  
  if(debug){
    Serial1.print("X: ");
    Serial1.print(xAxisVal);
    Serial1.print("  Y: ");
    Serial1.print(yAxisVal);
    Serial1.print("   SS: ");
    Serial1.print(stepspd);
    Serial1.print("  SPD: ");
    Serial1.print(spd);
    Serial1.print("  Dir: ");
    Serial1.print(dir);
    Serial1.print("  Shut: ");
    Serial1.print(shutter);
    Serial1.print("  Bulb: ");
    Serial1.print(bulb);
    Serial1.print("  CC: ");
    Serial1.print(camSel);
    Serial1.print("     ");
  }
  
  data = "BS" + (String)stepspd;
  data += "D" + (String)dir;
  data += "P" + (String)spd;
  data += "K" + (String)shutter;
  data += "L" + (String)bulb;
  data += "M" + (String)camSel;
  
  char charBuf[data.length() + 1];
  data.toCharArray(charBuf, data.length() +1 );
  int datalength = 0;
  for(int loopCntr=0; loopCntr <= data.length(); loopCntr++){
     datalength += int(charBuf[loopCntr]);
  }
  datalength += data.length();  
  data += "C" + (String)datalength;
  data += "E";
  
  if(debug){
    //Serial1.print("Data: ");
    //Serial1.println(data);
  }
  
  data.toCharArray(charBuf,data.length() + 1);
  digitalWrite(statLedPin, LOW);
  if(data != prevData){
    digitalWrite(statLedPin, HIGH);
    prevData = data;
    
    nrf_send(charBuf);
    
    Serial1.print("Data: ");
    Serial1.println(data);
  }
  
  if(camSel == LOW && shutter == 1){
    if(bulb > 0){
      bulbtmp = bulb;
      datatmp = data;
      String datanew = data;
      
      //Serial1.println("DATATMP: " + data);
      
      int lstr = datatmp.indexOf("L");      
      //Serial1.println("LSTR: " + (String)lstr);
      
      int mstr = datatmp.indexOf("M") + 1;
      //Serial1.println("MSTR: " + (String)mstr);
      
      bulbstr = datatmp.substring(lstr,mstr);
      //Serial1.println("BULBSTR: " + (String)bulbstr);
      
      int iii = 0;
      
      if(bulb > 1){
       bulb--; 
      }
      
      for(int i = bulb; i >= 1; i--){
        for(int x = 0; x < 10; x++){
          digitalWrite(statLedPin, LOW);
          delay(45);
          digitalWrite(statLedPin, HIGH);
          delay(49);
        }
        
        iii = i - 1;
        
        Serial1.println("BULB: " + (String)iii);
        
        datatmp.replace(bulbstr,"L" + (String)iii + "M");
        bulbstr = "L" + (String)iii + "M";
        //Serial1.print("DataTMP: ");
        //Serial1.println(datatmp);
        datatmp.toCharArray(charBuf,datatmp.length() + 1);
        nrf_send(charBuf);
        
      }
      data.toCharArray(charBuf,data.length() + 1);
      nrf_send(charBuf);
    }
  }else{
    delay(1);
  }
   
//  if(nRF_receive() != "ACK"){
//    if(debug){
//      Serial1.println("HELLO WHERE ARE YOU");
//    }
    nrf_send("hand");
//  }
   
  
  if(debug){
    Serial1.println();
  }
  
} // end loop()





void nrf_send(char* data){

  strcat(SendPayload,data);
  // swap TX & Rx addr for writing
  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(0,pipes[0]);
  radio.stopListening();
  bool ok = radio.write(&SendPayload,strlen(SendPayload));
  if(debug){
    Serial1.print(" Sent: ");
    Serial1.print((String)SendPayload);
    Serial1.println("  ");
  }

  // restore TX & Rx addr for reading
  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);
  radio.startListening();
  SendPayload[0] = 0;
  dataBufferIndex = 0;  
}


String nRF_receive(void) {
  int len = 0;
  String rxcv = "";
  
  if ( radio.available() ) {
      boolean done = false;
      while ( !done ) {
        len = radio.getDynamicPayloadSize();
        done = radio.read(&RecvPayload,len);
        delay(5);
      }
    rxcv = (String)RecvPayload;
    RecvPayload[len] = 0; // null terminate string
    if(debug){
      Serial1.print(" Rcvd: ");
      //.print((String)RecvPayload);
      //Serial.println("  ");
    }
    RecvPayload[0] = 0; // Clear the buffers
    
    return rxcv;
  }

} // end nRF_receive()
