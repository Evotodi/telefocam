/*
TeleFoCam Comm Controller (base)

Controller: Arduino Uno
Date : 2/19/2014
Version : 1.0
*/

#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"
#include <SoftwareSerial.h>

RF24 radio(9,10);
SoftwareSerial myserial(2, 3); // RX, TX

const boolean debug = false;

const uint64_t pipes[2] = { 0xDEDEDEDEE7LL, 0xDEDEDEDEE9LL };

boolean stringComplete = false; // whether the string is complete
static int dataBufferIndex = 0;
boolean stringOverflow = false;
char charOverflow = 0;

char SendPayload[31] = "";
char RecvPayload[31] = "";
char serialBuffer[31] = "";

String serstring = ""; 
boolean newlinesent = false;
boolean startup = true;
boolean sent = false;
boolean masterRdy = false;
String data = "BS0D0P1J1K1N0"; //CxE

const int analogInPin = A0;
const int redLED = A1;
const int yelLED = A2;
const int grnLED = A3;

int battAnalog = 0;
int battVolt;

void setup(void) { 
    pinMode(redLED,OUTPUT);
    pinMode(yelLED,OUTPUT);
    pinMode(grnLED,OUTPUT);
    
    Serial.begin(57600); //debug comms
    myserial.begin(57600); //comms to netduino
    Serial.println("Comm Controller Begin");
    radio.begin(); //comms to hand ctrl
    printf_begin();
    
    digitalWrite(redLED,HIGH);
    digitalWrite(yelLED,HIGH);
    digitalWrite(grnLED,HIGH);
    delay(500);
    digitalWrite(redLED,LOW);
    digitalWrite(yelLED,LOW);
    digitalWrite(grnLED,LOW);    
    
    radio.setDataRate(RF24_250KBPS);
    radio.setPALevel(RF24_PA_MAX);
    radio.setChannel(70);
    
    radio.enableDynamicPayloads();
    radio.setRetries(15,15);
    radio.setCRCLength(RF24_CRC_16);
    
    radio.openWritingPipe(pipes[0]);
    radio.openReadingPipe(1,pipes[1]);
    
    radio.startListening();
    radio.printDetails();
    
    Serial.println("Waiting fo hand controller");
    
    boolean waithand = false;
    while(!waithand){
      strcat(SendPayload,"base");
      // swap TX & Rx addr for writing
      radio.openWritingPipe(pipes[1]);
      radio.openReadingPipe(0,pipes[0]);
      radio.stopListening();
      bool ok = radio.write(&SendPayload,strlen(SendPayload));
      if(debug){
        Serial.print("Sent:");
        Serial.print(SendPayload);
        Serial.println();
      }
      stringComplete = false;
    
      // restore TX & Rx addr for reading
      radio.openWritingPipe(pipes[0]);
      radio.openReadingPipe(1,pipes[1]);
      radio.startListening();
      SendPayload[0] = 0;
      dataBufferIndex = 0;  
      
      delay(250);
      
      int len = 0;
      if ( radio.available() ) {
          bool done = false;
          while ( !done ) {
            len = radio.getDynamicPayloadSize();
            done = radio.read(&RecvPayload,len);
            delay(5);
          }
      
        RecvPayload[len] = 0; // null terminate string
        
        if(debug){
          Serial.print("Rcv:");
          Serial.print(RecvPayload);
          Serial.println();
        }
        
        if((String)RecvPayload == "hand"){
          Serial.println("Received hand contoller ready."); 
          waithand = true;
        }   
        
        RecvPayload[0] = 0; // Clear the buffers
      }
      
      delay(5);
      checkBatt(); 
    }
    
    
    Serial.println("Waiting for master controller"); 
    boolean waitbase = false;
    while(!waitbase){
      if(myserial.available()){
        while (myserial.available()) {
          char inchar = (char)myserial.read();
          serstring += inchar;
          if (inchar == '\r' || inchar == '\n') {newlinesent = true;}
        }
      }    
      if (newlinesent){
        Serial.print("Reply:" + serstring);      
        newlinesent = false;
        if (serstring == "master\n"){
          Serial.println("Found Master Controler"); 
          myserial.println("comms");

        }
        if (serstring == "a\n"){
          Serial.println("Master Controler Ready"); 
          waitbase = true;
        }
        serstring = "";
      } 
      delay(5);
    }
    
    Serial.println("Comm Controller Setup Complete");
    Serial.println("Proceeding to RUN mode");
    
}

void loop(void) {
  int st = 0;
  int ed = 0;
  String nrfData = "";
  String newData = "";
  String chksum = "";
  int datalength = 0;
  
  nrfData = nRF_receive();
  
  if(nrfData.startsWith("B")){
    
    ed = nrfData.indexOf("C");
    newData = nrfData.substring(0,ed); 
    chksum = nrfData.substring(ed - 1);
    
    char charBuf[newData.length() + 1];
    newData.toCharArray(charBuf, newData.length() +1 );
    
    for(int loopCntr=0; loopCntr <= newData.length(); loopCntr++){
       datalength += int(charBuf[loopCntr]);
       //Serial.println(int(charBuf[loopCntr]));
    }
    datalength += newData.length(); 
    
    if((String)datalength = chksum){
      if(debug){
        //Serial.println("GOOD DATA");
      } 
      if(data != nrfData){
        data = nrfData;
        
        //send to netduino here----------------------------------------------------------------------<<<<<<<<<<<<<


        data.trim();
        myserial.println(data);
        Serial.println("Sent: " + data);
          
//          if(myserial.available()){
//            while (myserial.available()) {
//              char inchar = (char)myserial.read();
//              serstring += inchar;
//              if (inchar == '\r' || inchar == '\n') {
//                newlinesent = true;
//              }
//            }
//          }
        //}
        
        
        
        //send to netduino here----------------------------------------------------------------------<<<<<<<<<<<<<        
        
        //Serial.println((String)data);
      }
    }
    
  }  
  checkBatt();
} // end loop()

String nRF_receive(void) {
  int len = 0;
  String nRFData;
  if ( radio.available() ) {
      bool done = false;
      while ( !done ) {
        len = radio.getDynamicPayloadSize();
        done = radio.read(&RecvPayload,len);
        delay(5);
      }
  
    RecvPayload[len] = 0; // null terminate string
    if(debug){
      Serial.print("R:");
      Serial.print(RecvPayload);
      Serial.println();
    }
    nRFData = RecvPayload;
    RecvPayload[0] = 0; // Clear the buffers
    return nRFData;
  }

} // end nRF_receive()

void nrf_send(char* data){

  strcat(SendPayload,data);
  // swap TX & Rx addr for writing
  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(0,pipes[0]);
  radio.stopListening();
  bool ok = radio.write(&SendPayload,strlen(SendPayload));
  if(debug){
    Serial.print("Sent:");
    Serial.print(SendPayload);
    //Serial1.print("       \r");
  }

  // restore TX & Rx addr for reading
  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);
  radio.startListening();
  SendPayload[0] = 0;
  dataBufferIndex = 0;  
}

void checkBatt(void){
   battAnalog = analogRead(analogInPin);
  battVolt = map(battAnalog, 0, 1023, 0, 16);
  
  if(debug){
    Serial.print("Analog = " );                       
    Serial.print(battAnalog);      
    Serial.print(" MAP = ");      
    Serial.print(battVolt);  
    Serial.println(""); 
  }
  switch (battVolt){
    case 16:
    case 15:
    case 14:
      digitalWrite(redLED,HIGH);
      digitalWrite(yelLED,HIGH);
      digitalWrite(grnLED,HIGH);
      break;
    case 13:
    case 12:
    case 11:
      digitalWrite(redLED,LOW);
      digitalWrite(yelLED,LOW);
      digitalWrite(grnLED,HIGH);
      break;
    case 10:
      digitalWrite(redLED,LOW);
      digitalWrite(yelLED,HIGH);
      digitalWrite(grnLED,LOW);
      break;
    default:    
      digitalWrite(redLED,HIGH);
      digitalWrite(yelLED,LOW);
      digitalWrite(grnLED,LOW);
  }  
}//end checkBatt

